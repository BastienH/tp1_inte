package fr.elfoa.drone;

import static org.junit.Assert.assertEquals;

import fr.elfoa.AbstractBootstraper;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Pierre Colomb
 */
public class DroneTest extends AbstractBootstraper{

    private static final Point ORIGIN = new Point(0d,0d,0d);

    @BeforeClass
    public static void start(){
        init();
    }

    @Test
    public void tackOff() throws Exception {

        //Drone drone = new Drone(ORIGIN);

        Drone drone = getInstance(Drone.class);

        drone.tackOff();

        assertEquals(50d,drone.getCurrentPosition().getAltitude(),0);



    }



    @Test
    public void flyTo() throws Exception {
        //Drone drone = new Drone(ORIGIN);
        Drone drone = getInstance(Drone.class);

        drone.tackOff();
        drone.flyTo(new Point(10d, 10d, 10d));

        assertEquals(10d, drone.getCurrentPosition().getLatitude(), 0);
        assertEquals(10d, drone.getCurrentPosition().getLongitude(), 0);
        assertEquals(10d, drone.getCurrentPosition().getAltitude(), 0);

    }



    @Test
    public void landing() throws Exception {
        //Drone drone = new Drone(ORIGIN);
        Drone drone = getInstance(Drone.class);

        drone.tackOff();
        drone.flyTo(new Point(10d, 10d, 10d));
        drone.landing();

        assertEquals(10d, drone.getCurrentPosition().getLatitude(), 0);
        assertEquals(10d, drone.getCurrentPosition().getLongitude(), 0);
        assertEquals(0d, drone.getCurrentPosition().getAltitude(), 0);
    }



    @Test
    public void isCanFly() throws Exception {
        //Drone drone = new Drone(ORIGIN);
        Drone drone = getInstance(Drone.class);

        assertEquals(true, drone.isCanFly());
    }



    @Test
    public void getCurrentPosition() throws Exception {
        Drone drone = new Drone(new Point(10d, 10d, 0d));

        assertEquals(10d, drone.getCurrentPosition().getLatitude(), 0);
        assertEquals(10d, drone.getCurrentPosition().getLongitude(), 0);
        assertEquals(0d, drone.getCurrentPosition().getAltitude(), 0);
    }

    @AfterClass
    public static void stop(){
        shutdown();
    }
}