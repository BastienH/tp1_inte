package fr.elfoa.drone;


public class ModuleLithiumIon implements IModule {

    private Integer power = 100;

    public ModuleLithiumIon(){}

    @Override
    public void use(Integer power) {
        this.power -= 3*power/4;
    }

    @Override
    public Integer getPower() {
        return this.power;
    }
}
