package fr.elfoa.drone;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * @author Pierre Colomb
 */
public class Battery {
    
    private List<ModuleLithiumOxygen> modules = new ArrayList<>();

    public Battery(){
        modules = Arrays.asList(new ModuleLithiumOxygen(),
                                new ModuleLithiumOxygen(),
                                new ModuleLithiumOxygen(),
                                new ModuleLithiumOxygen());
    }

    void use(Integer power){
        ModuleLithiumOxygen module = modules.stream()
                               .filter(m -> m.getPower() != 0)
                               .findFirst()
                               .orElseThrow(UnsupportedOperationException::new);

        module.use(power);

    }

    Integer getPower(){
        return modules.stream()
                      .mapToInt(ModuleLithiumOxygen::getPower)
                      .sum();
    }

}
