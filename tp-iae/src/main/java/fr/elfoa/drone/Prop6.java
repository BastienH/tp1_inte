package fr.elfoa.drone;
import javax.inject.*;
import java.lang.annotation.*;

import static java.lang.annotation.RetentionPolicy.*;

@Qualifier
@Retention(RUNTIME)
public @interface Prop6 {
}
