package fr.elfoa.drone;

import javax.inject.Inject;

@Prop8
public class Propellers8 implements IPropellers {
    private Integer number = 8;

    private Battery battery;

    private ConsumptionCalculator calculator = new ConsumptionCalculator();

    private Boolean isRunning = false;

    @Inject
    public Propellers8(Battery battery){
        this.battery = battery;
    }

    @Override
    public void start() {
        battery.use(calculator.getConsumption(number));
        isRunning = true;
    }

    @Override
    public void stop() {
        isRunning = false;
    }

    @Override
    public Integer getNumberOfPropelle() {
        return number;
    }

    @Override
    public Boolean getRunning() {
        return isRunning;
    }
}
