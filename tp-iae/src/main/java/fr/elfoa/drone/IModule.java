package fr.elfoa.drone;

public interface IModule {

    public void use(Integer power);

    public Integer getPower();
}
