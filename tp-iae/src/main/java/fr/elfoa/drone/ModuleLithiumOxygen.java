package fr.elfoa.drone;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Oxygen
public class ModuleLithiumOxygen implements IModule {
    private Integer power = 100;

    public ModuleLithiumOxygen(){}

    @Override
    public void use(Integer power) {
        this.power -= power/2;
    }

    @Override
    public Integer getPower() {
        return this.power;
    }
}
