package fr.elfoa.drone;

import java.lang.annotation.Retention;


public interface IPropellers {
    public void start();

    public void stop();

    public Integer getNumberOfPropelle();

    public Boolean getRunning();

}
