package fr.elfoa.drone;

import javax.inject.Inject;

@Prop6
public class Propellers6 implements IPropellers {

    private Integer number = 6;

    private IModule battery;

    private ConsumptionCalculator calculator = new ConsumptionCalculator();

    private Boolean isRunning = false;

    @Inject
    public Propellers6(IModule battery){
        this.battery = battery;
    }

    @Override
    public void start() {
        battery.use(calculator.getConsumption(number));
        isRunning = true;
    }

    @Override
    public void stop() {
        isRunning = false;
    }

    @Override
    public Integer getNumberOfPropelle() {
        return number;
    }

    @Override
    public Boolean getRunning() {
        return isRunning;
    }
}

